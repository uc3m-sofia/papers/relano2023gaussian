e-cienciaDatos: "readme" form 

-------------------
GENERAL INFORMATION
-------------------
1. Title of dataset

SoftGP: Gaussian process regression for forward and inverse kinematics of soft robots

https://doi.org/10.21950/Y4AN3E

2. Contact

   Principal Investigator Contact Information
        Name: Concepción Alicia Monje Micharet
        Institution: University Carlos III of Madrid
        Email: cmonje@ing.uc3m.es
	ORCID: 0000-0001-8295-127X

3. Description of the project

Modeling the forward and inverse kinematics of a soft robot arm and neck using Gaussian processes


4. Description of the dataset

The data collected from the soft arm and neck and the Gaussian process models trained using that data.


5. Notes

The models are described here:

Relaño, C., Muñoz, J., Monje, C. A. (2023). Gaussian process regression for forward and inverse kinematics of a soft robotic arm, Engineering Applications of Artificial Intelligence, 126 (107174), Part D, https://doi.org/10.1016/j.engappai.2023.107174.
(https://www.sciencedirect.com/science/article/pii/S0952197623013581)


6. Deposit date


7. Date


8. Language

English

--------------------------
AUTHOR INFORMATION
--------------------------
1. Author

        Name: Carlos 
        Last name  : Relaño
        Institution: Universidad Carlos III de Madrid
        Email: crelano@ing.uc3m.es
	ORCID: https://orcid.org/0000-0002-3276-4668

        Name: Javier
        Last name  : Muñoz
        Institution: Universidad Carlos III de Madrid
        Email: jamunozm@ing.uc3m.es
	ORCID: https://orcid.org/0000-0001-5068-3303

        Name: Concepción A.
        Last name  : Monje
        Institution: University Carlos III de Madrid
        Email: cmonje@ing.uc3m.es
	ORCID: https://orcid.org/0000-0001-8295-127X


--------------------------
METHODOLOGY
--------------------------
1. Methodology

Usage:

Create a python environment with version Python 3.10.9. Then install the necessary libraries to run the notebooks with the command: 

pip install -r requirements.txt

Run the file main.py. There you can select the GP model to use (ExactGP, AGP, DGP1, DGP2, DGP3) and the soft robot to use (arm, neck).


2. Software

Python
 
--------------------------
KEYWORDS
--------------------------
1. Keywords

Soft robotics, Gaussian processes, Machine learning, Identification of soft robots

--------------------------
SPONSORSHIP INFORMATION AND GRANT IDs
--------------------------
1. Grant Information

This research has been supported by the project SOFIA, with reference PID2020-113194GB-I00, funded by the Spanish Ministry of Economics, Industry and Competitiveness (MCIN/ AEI /10.13039/501100011033), and the project Desarrollo de articulaciones blandas para aplicaciones robóticas, with reference IND2020/IND-1739, funded by the Community of Madrid (CAM) (Department of Education and Research). 

--------------------------
RELATED PUBLICATIONS
--------------------------
1. Related publication

Relaño, C., Muñoz, J., Monje, C. A. (2023). Gaussian process regression for forward and inverse kinematics of a soft robotic arm, Engineering Applications of Artificial Intelligence, 126, Part D,
107174, https://doi.org/10.1016/j.engappai.2023.107174.
(https://www.sciencedirect.com/science/article/pii/S0952197623013581)

2. Related dataset


--------------------------
GEOGRAPHIC INFORMATION
--------------------------
1. Spatial coverage


--------------------------
TEMPORAL INFORMATION
--------------------------
1. Time period coverage



--------------------------
FILES
--------------------------
1. Files 

softGP.zip = compressed folder of files with data

	requirements.txt = python libraries required to run the scripts
	README.md = documentation
	dataArm.csv = data collected from the soft robot arm used to train the GP models
	dataNeck.csv = data collected from the soft robot neck used to train the GP models
	main.py = main python script to run predictions from the GP models. More 			information and the detailed steps with an example to make predictions with the GP 		models are inside.
	soft_gp = folder with python classes and functions to interact with the trained models 		via main.py
 	LICENSE.md = license

softgp_2023_v01_readme.txt = documentation 

--------------------------
LICENSES AND PRIVACITY
--------------------------
1. Licenses

Creative Commons CC-BY-NC-ND

2. Privacity


--------------------------
OTHERS
--------------------------
1. Data dictionary



# softGP
Gaussian process models for the forward and inverse kinematics of the soft arm and neck developed in the UC3M RoboticsLab


Python 3.10.9

To install the necessary libraries:

pip install -r requirements.txt

It is recommended to perform the installation on a separate conda environment.
